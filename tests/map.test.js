const {map, cb} = require("../map.cjs")


const items = [1, 2, 3, 4, 5, 5];

test('Testing map',() => {
    expect(map(items,cb)).toStrictEqual([ 2, 4, 6, 8, 10, 10 ]);
})