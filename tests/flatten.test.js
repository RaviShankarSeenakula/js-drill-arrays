const flatten = require("../flatten.cjs")

const array2 = [1,, 3, ["a",, ["d",, "e"]]];

test('Testing flatten',() => {
    expect(flatten(array2, 2)).toStrictEqual([ 1, 3, 'a', 'd', 'e' ])
})