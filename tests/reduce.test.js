const {reduce, cb} = require("../reduce.cjs")


const items = [1, 2, 3, 4, 5, 5];

test('Testing reduce',() => {
    expect(reduce(items, cb, 0)).toBe(20)
})