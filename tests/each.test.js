const each = require("../each.cjs")

const items = [1, 2, 3, 4, 5, 5];

test('Testing each',() => {
    expect(each(items)).toStrictEqual([1, 4, 9, 16, 25, 25])
})