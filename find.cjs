// Do NOT use .includes, to complete this function.
// Look through each value in `elements` and pass each element to `cb`.
// If `cb` returns `true` then return that element.
// Return `undefined` if no elements pass the truth test.


const items = [1, 2, 3, 4, 5, 5];

function cb(ele , value) {
    if (ele === value) {
        return true;
    }
    return false;
}

function find(elements, value) {

    for (let index = 0; index < elements.length; index++) {
        if (cb(elements[index], value) === true) {
            return value;
        }
    }
    return undefined;
}

// console.log(find(items,4));

module.exports = find;
