// Do NOT use .reduce to complete this function.
// How reduce works: A reduce function combines all elements into a single value going from left to right.
// Elements will be passed one by one into `cb` along with the `startingValue`.
// `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
// `startingValue` is the starting value.  If `startingValue` is undefined then make `elements[0]` the initial value.


const items = [1, 2, 3, 4, 5, 5];


function cb(startingValue, ele, index, elements) {

    return startingValue + ele;

}

function reduce(elements, cb, startingValue) {
    if (!Array.isArray(elements)) {
        return [];
    }

    for (let index = 0; index < elements.length; index++) {
        if (startingValue === undefined) {
            startingValue = elements[index];
        } else {
            startingValue = cb(startingValue, elements[index], index, elements);
        }
    }

    return startingValue;
}


console.log(reduce(items,cb));

module.exports = reduce;