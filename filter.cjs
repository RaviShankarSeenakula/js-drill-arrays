
// Do NOT use .filter, to complete this function.
// Similar to `find` but you will return an array of all elements that passed the truth test
// Return an empty array if no elements pass the truth test


const items = [1, 2, 3, 4, 5, 5];


function cb(ele, index, elements) {
    if (ele < 0)
        return true;
    return false;
}


function filter(elements, cb) {

    const result = [];
    
    for (let index = 0; index < elements.length; index++) {
        if (cb(elements[index], index, elements) === true) {
            result.push(elements[index]);
        }
    }

    return result;
}


// console.log(filter(items))

module.exports = filter;