const nestedArray = [1, [2], [[3]], [[[4]]]];
const array2 = [1,, 3, ["a",, ["d",, "e"]]];
const array = [1,'4',[['']],2,3,4];
// Flattens a nested array (the nesting can be to any depth).
// Hint: You can solve this using recursion.
// Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];

// const result = [];

// function flatten(elements, depth = 1) {

//     // if(elements === undefined || elements === '') {
//     //     return;
//     // }

//     if (depth < 1) {
//         return elements;
//     }

//     for (let index = 0; index < elements.length; index++) {

//         if (Array.isArray(elements[index])) {
//             if (depth > 0) {
//                 flatten(elements[index], depth - 1)
//             } else {
//                 if (elements[index]) {
//                     result.push(elements[index]);
//                 }
//             }
//         } else {
//             if (elements[index]) {
//                 result.push(elements[index]);
//             }
//         }
//     }
//     return result;
// }





const result = [];

function flatten(elements, depth = 1) {
    for (let index = 0; index < elements.length; index++) {
        if (Array.isArray(elements[index])) {
            if (depth > 0)
            {
                flatten(elements[index], depth - 1)
            } else {
                if (elements[index] != undefined) {
                    result.push(elements[index]);
                }
            }
        } else {
            if (elements[index] != undefined) {
                result.push(elements[index]);
            }     
        }
    }
    return result;
}


// console.log(flatten(nestedArray,2));
// console.log(nestedArray.flat(2));

module.exports = flatten;