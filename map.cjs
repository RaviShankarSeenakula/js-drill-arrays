// Do NOT use .map, to complete this function.
// How map works: Map calls a provided callback function once for each element in an array, in order, and functionructs a new array from the res .
// Produces a new array of values by mapping each value in list through a transformation function (iteratee).
// Return the new array.

const items = [1, 2, 3, 4, 5, 5];


function cb(ele, index, elements) {
    return ele + ele;
}

function map(elements, cb) {
    if (!Array.isArray(elements)) {
        return [];
    }

    let result = [];

    for (let index = 0; index < elements.length; index++) {
        if (Array.isArray(elements)) {
            result.push(cb(elements[index], index, elements));
        } else {
            result.push(cb({key , value}, index, elements));
        }
        
    }
    return result;
}

module.exports = map;