const items = [1, 2, 3, 4, 5, 5];

function cb(ele, i) {
    return ele * ele;
}

function each(element) {
    if (!Array.isArray(element)) {
        return [];
    }

    let result = [];

    for (let index = 0; index < element.length; index++) {
        result.push(cb(element[index], index));
    }
    return result;
}

// console.log(each(items));

module.exports = each;




// Do NOT use forEach to complete this function.
// Iterates over a list of elements, yielding each in turn to the `cb` function.
// This only needs to work with arrays.
// You should also pass the index into `cb` as the second argument
// based off http://underscorejs.org/#each